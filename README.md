# Dashboard .NET - Angular 

This project was generated with Angular CLI version 8.0.0.

# Dev

   Correr ng serve para iniciar el dev server de Angular, dirección http://localhost:4200/.

   Probar API en un ambiente propio: Se implementó la UI Swagger, para probarlo, ejecutár la aplicación y dirigirse en la URL a la dirección /swagger


# Proyecto
Dashboard en el que se representan datos, tablas y charts, el diseño de estos últimos fueron tomados a partir de la libreria de JavsScript 'GraphicsJS'.

	El Front End fue realizado en Angular CLI 8.x
	El Back End fue un desarrollo de una API RESTful realizada en .NET Core 2.2 diseñado bajo el patrón de diseño MVC, con posibilidad de agrandar el desarrollo y utilizar el patrón MVVM.
	La BBDD utilizada fue desarrollada en SQL Server y se mapea con Entity Framework (Aunque evalué la posibilidad de implementar el mapeo de las tablas mediante Stored Procedures y adaptar la forma en que necesitaba los datos)


El front consume la API realizada en .NET para generar datos dinámicos y mostrarlos en el componente correspondiente.