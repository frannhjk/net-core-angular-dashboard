﻿using angCoreAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace angCoreAPI
{
    public class DataSeed
    {
        private readonly ApplicationDbContext _ctx; //referencia a nuesto context(bbbdd)

        public DataSeed(ApplicationDbContext ctx)
        {
            _ctx = ctx;
        }

        //Popular Customer, Order, Server
        public void SeedData(int nCustomers, int nOrders)
        {
            // si no existen customers en la BBDD
            if (!_ctx.Customers.Any()) 
            {
                seedCustomers(nCustomers);
                _ctx.SaveChanges();
            }

            // si no existen orders en la BBDD
            if (!_ctx.Orders.Any()) 
            {
                seedOrders(nOrders);
                _ctx.SaveChanges();
            }

            // si no existen servers en la BBDD
            if (!_ctx.Servers.Any()) 
            {
                seedServers();
                _ctx.SaveChanges();
            }
        }

        // ############################## Customers #######################################

        private void seedCustomers(int n)
        {
            //declaro colección y la lleno con el método BuildCustomerList()
            List<Customer> customers = BuildCustomerList(n);

            //recorro la colección y agrego customers
            foreach(var customer in customers)
            {
                _ctx.Customers.Add(customer);
            }
        }

        private List<Customer> BuildCustomerList(int nCustomer)
        {
            var customers = new List<Customer>();
            var namesList = new List<string>();

            for(var i = 1; i <= nCustomer; i++)
            {
                var name = Helpers.MakeUniqueCustomerName(namesList);
                
                namesList.Add(name);

                var email = Helpers.MakeCustomerEmail(name);

                var city = Helpers.MakeCustomerCity();

                //instancio nuevos Customer a la lista
                customers.Add(new Customer
                {
                       Id = i,
                       Name = name,
                       Email = email,
                       City = city
                });
            }
            return customers;
        }


        // ############################## Orders #######################################

        private void seedOrders(int n)
        {
            List<Order> orders = BuildOrderList(n);

            foreach (var order in orders)
            {
                _ctx.Orders.Add(order);
            }
        }

        private List<Order> BuildOrderList(int nOrders)
        {
            var orders = new List<Order>();
            var rand = new Random();

            for (var i = 1; i <= nOrders; i++)
            {
                var randCustomerId = rand.Next(_ctx.Customers.Count());
                var placed = Helpers.GetRandomOrderPlaced();
                var completed = Helpers.GetRandomOrderCompleted(placed);
                var customers = _ctx.Customers.ToList();

                orders.Add(new Order
                {
                    Id = i,
                    Custumer = customers.First(c => c.Id == randCustomerId),
                    Total = Helpers.GetRandomOrderTotal(),
                    Placed = placed,
                    Fulfilled = completed
                });
            }

            return orders;
        }


        // ############################## Servers #######################################
        private void seedServers()
        {
            List<Server> servers = BuildServerList();

            foreach (var server in servers)
            {
                _ctx.Servers.Add(server);
            }
        }

        private List<Server> BuildServerList()
        {
            var servers = new List<Server>()
            {
                new Server
                {
                    Id = 1,
                    Name = "Servidor 1 Producción",
                    isOnline = true
                }, 
                new Server
                {
                    Id = 2,
                    Name = "Servidor 2 Testing",
                    isOnline = true
                },  
                new Server
                {
                    Id = 3,
                    Name = "Servidor 3 Desarrollo",
                    isOnline = false
                },  
                new Server
                {
                    Id = 4,
                    Name = "Servidor 4 QC",
                    isOnline = true
                },  
                new Server
                {
                    Id = 5,
                    Name = "Servidor 5 QA",
                    isOnline = false
                }

            };

            return servers;
        }
    }
}
