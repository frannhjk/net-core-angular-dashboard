﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using angCoreAPI.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Swagger;

namespace angCoreAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            //Para ejecutar varios localhost 
            services.AddCors(option => {
                    option.AddPolicy("CorsPolicy", 
                    c => c.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());
            });


            services.AddDbContext<ApplicationDbContext>(options =>
            {
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) //si se ejecuta en windows, else {sqlite}, en, ejemplo, linux
                {
                    options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
                }

            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            // services.AddSwaggerGen(options =>
            //     options.SwaggerDoc("v1", new Info { Title = "Dashboard API", Version = "v1" })
            // );

            
            //services.AddTransient<DataSeed>();

        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            // app.UseSwagger();

            // app.UseSwaggerUI(options =>
            //     options.SwaggerEndpoint("/swagger/v1/swagger.json", "Dashboard API v1")
            // );

            // app.Run(context =>
            // {
            //    context.Response.Redirect("/swagger");
            //    return Task.CompletedTask;
            // });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseCors("CorsPolicy");
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            
            app.UseHttpsRedirection();

            //seed.SeedData(20, 1000);

            app.UseMvc(routes => routes.MapRoute(
                "default","api/{controller}/{action}/{id}"
                
            ));
        }
    }
}
