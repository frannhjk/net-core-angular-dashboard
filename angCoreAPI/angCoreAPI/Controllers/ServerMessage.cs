namespace angCoreAPI.Controllers
{
    public class ServerMessage
    {
        public int Id {get; set;}
        public string Payload {get; set;}
    }
}