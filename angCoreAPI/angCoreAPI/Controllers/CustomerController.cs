﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using angCoreAPI.Models;
using Microsoft.AspNetCore.Mvc;

namespace angCoreAPI.Controllers
{
    [Route("api/[controller]")]
    public class CustomerController : Controller
    {
        private readonly ApplicationDbContext _ctx;

        public CustomerController(ApplicationDbContext ctx)
        {
            _ctx = ctx;
        }


        // GET api/customer
        [HttpGet]
        public IActionResult Get()
        {
            var data = _ctx.Customers.OrderBy(c => c.Id);

            return Ok(data);
        }

        // GET api/customer/5
        [HttpGet("{id}", Name = "GetCustomer")]
        public IActionResult Get(int id)
        {
            var customer = _ctx.Customers.Find(id);
            return Ok(customer);
        }

        [HttpPost]
        public IActionResult Post([FromBody] Customer customer)
        {
            if (customer == null)
                return BadRequest();

            _ctx.Customers.Add(customer);
            _ctx.SaveChanges();

            //201 sucessfuly created, al método GET le paso el id y el objeto
            return CreatedAtRoute("GetCustomer", new { id = customer.Id }, customer);
        }
    }
}