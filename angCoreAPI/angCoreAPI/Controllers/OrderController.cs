﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using angCoreAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace angCoreAPI.Controllers
{
    [Route("api/[controller]")]
    public class OrderController : Controller
    {
        private readonly ApplicationDbContext _ctx;

        public OrderController(ApplicationDbContext ctx)
        {
            _ctx = ctx;
        }


        // GET  api/order/numeroPagina/tamañoPagina
        [HttpGet("{pageIndex:int}/{pageSize:int}")]
        public IActionResult Get(int pageIndex, int pageSize)
        {
            // Incluyo customer en el query result
            var data = _ctx.Orders.Include(o => o.Custumer)
                .OrderByDescending(c => c.Placed);

            // Instancio clase PaginatedResponse ()
            var page = new PaginatedResponse<Order>(data, pageIndex, pageSize);

            var totalCount = data.Count();
            var totalPages = Math.Ceiling((double)totalCount/pageSize);

            var response = new
            {
                Page = page,
                TotalPages = totalPages
            };

            return Ok(response);
        }

        //GET orders by ciudad
        [HttpGet("ByCity")]
        public IActionResult ByCity()
        {
            //obtengo Ordenes e incluyo los Customers
            var orders = _ctx.Orders.Include(o => o.Custumer).ToList();

            //group by city
            var groupResult = orders.GroupBy(o => o.Custumer.City)
                .ToList()
                .Select(grp => new //de la query de arriba obtenemos un nuevo objeto con City y Total
                {
                    City = grp.Key, //key del grouping (City)
                    Total = grp.Sum(x => x.Total)
                }).OrderByDescending(res => res.Total) //ordenamos el total de todo por total
                .ToList(); 

            return Ok(groupResult);
        }

        //GET orders by customer, por ejemplo le agrego parámetros para tener un número máximo de resultados!
        [HttpGet("ByCustomer/{n}")]
        public IActionResult ByCustomer(int n)
        {
            var orders = _ctx.Orders.Include(o => o.Custumer).ToList();

            var groupResult = orders.GroupBy(o => o.Custumer.Id)
                .ToList()
                .Select(grp => new
                {
                    Name = _ctx.Customers.Find(grp.Key).Name,  //obtengo el nombre específico del customer con el key del group by (Id)
                    Total = grp.Sum(x => x.Total)
                }).OrderByDescending(res => res.Total)
                .Take(n)    //número maximo de resultados
                .ToList();

            return Ok(groupResult);
        }

        [HttpGet("GetOrder/{id}", Name = "GetOrder()")]
        public IActionResult GetOrder(int id)
        {
            //retorno el Primer orden con su respectivo customer cuyo id sea igual al que se obtiene por párametro del método
             var order = _ctx.Orders
                .Include(o => o.Custumer)
                .First(o => o.Id == id);

                return Ok(order);
        }

    }
}