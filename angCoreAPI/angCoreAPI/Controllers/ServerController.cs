using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using angCoreAPI.Models;
using Microsoft.AspNetCore.Mvc;

namespace angCoreAPI.Controllers
{
    [Route("api/[controller]")]
    public class ServerController : Controller
    {
        private readonly ApplicationDbContext _ctx;

        //Constructor
        public ServerController(ApplicationDbContext ctx)
        {
            _ctx = ctx;
        }

        //GET
        [HttpGet]
        public IActionResult Get()
        {
            var response = _ctx.Servers.ToList();

            return Ok(response);
        }

        //GET by ID
        [HttpGet("{id}", Name="GetServer")]
        public IActionResult Get(int id)
        {
            var response = _ctx.Servers.Find(id);

            return Ok(response);
        }



        //Método para editar el estado de los servidores [PUT]
        [HttpPut("{id}")]
        public IActionResult Message(int id, [FromBody] ServerMessage msg)
        {
            var server = _ctx.Servers.Find(id);

            if(server == null)
            {
                return NotFound();
            }

            // Refactor: muevo al servicio
            if(msg.Payload == "activate")
            {
                server.isOnline = true;
            }
                
            
            if(msg.Payload == "desactivate")
            {
                server.isOnline = false;
            }

             _ctx.SaveChanges();

             return new NoContentResult();
        }

    }

}