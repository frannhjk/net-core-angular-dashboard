﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace angCoreAPI.Models
{
    public class Order
    {
        public int Id { get; set; }
        public Customer Custumer { get; set; } //cada orden pertenecerá a un customer
        public decimal Total { get; set; }
        public DateTime Placed { get; set; }
        public DateTime? Fulfilled { get; set; } //nullable
    }
}
