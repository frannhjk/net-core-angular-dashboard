﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace angCoreAPI
{
    public class Helpers
    {
        private static Random _rnd = new Random();


        private static string GetRandom(IList<string> items)
        {
            return items[_rnd.Next(items.Count())];
        }


        //############################## Customers #######################################
        internal static string MakeUniqueCustomerName(List<string> names) //recursiva
        {
            var maxNames = bizPrefix.Count + bizSuffix.Count;

            if(names.Count >= maxNames)
            {
                throw new System.InvalidOperationException("El máximo número para uniques nombres ha sido sobrepasado");
            }
            var prefix = GetRandom(bizPrefix);
            var suffix = GetRandom(bizSuffix);

            var bizName = prefix + suffix;

            if (names.Contains(bizName))
            {
                MakeUniqueCustomerName(names);
            }  
           
            return bizName;
        }

        private static readonly List<string> bizPrefix = new List<string>()
        {
            "ABC",
            "XYZ",
            "Orlando",
            "Machine",
            "D10",
            "Quilmes",
            "Brahma",
            "Stelo",
            "Whirlpool"
        };

        private static readonly List<string> bizSuffix = new List<string>()
        {
            "Corporation",
            "Co",
            "Transito",
            "AI",
            "Futbol",
            "Bebidas",
            "Bebidas",
            "Ren",
            "Electrodomésticos"
        }; 


        //Creo los emails de los customers
        internal static string MakeCustomerEmail(string customerNames)
        {
            return $"contact@{customerNames.ToLower()}.com";
        }

        //Creo las ciudades
        internal static string MakeCustomerCity()
        {
            return GetRandom(getRandomCity);
        }

        private static readonly List<string> getRandomCity = new List<string>()
        {
            "SAR", "CAP", "Are", "Ju", "San", "Arre", "Lo", "Is"
        };


        // ############################## Orders #######################################
        internal static decimal GetRandomOrderTotal()
        {
            return _rnd.Next(100, 5000);   
        }

        internal static DateTime GetRandomOrderPlaced()
        {
            var fin = DateTime.Now;
            var inicio = fin.AddDays(-90);

            TimeSpan ts = fin - inicio;
            TimeSpan nTs = new TimeSpan(0, _rnd.Next(0, (int)ts.TotalMinutes), 0);

            return inicio + nTs;
        }

        internal static DateTime? GetRandomOrderCompleted(DateTime orderPlaced)
        {
            var actual = DateTime.Now;
            var minTime = TimeSpan.FromDays(7);
            var timePassed = actual - orderPlaced;

            if(timePassed < minTime)
            {
                return null;
            }

            return orderPlaced.AddDays(_rnd.Next(7, 14));
        }

        

        //############################## Servers #######################################
    }
}
