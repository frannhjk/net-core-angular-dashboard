﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace angCoreAPI
{
    public class PaginatedResponse<T>
    {

        public PaginatedResponse(IEnumerable<T> data, int i, int len)
        {
            // skip de index - 1 y take la longitud como lista
            // página [1] quiero 10 resultados
            Data = data.Skip((i - 1) * len).Take(len).ToList();//ejemplo pagina[2] con 100 resultados
                                                               //Data = data.Skip((2 - 1) * len).Take(len).ToList();
            Total = data.Count();
        }

        public int Total { get; set; }
        public IEnumerable<T> Data { get; set; }
        
    }
}
