import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-paginacion',
  templateUrl: './paginacion.component.html',
  styleUrls: ['./paginacion.component.css']
})
export class PaginacionComponent implements OnInit {


  @Input() page: number;
  @Input() count: number;
  @Input() perPage: number;
  @Input() pagesToShow: number;
  @Input() loading: boolean;

  @Output() goPrev = new EventEmitter<boolean>();
  @Output() goNext = new EventEmitter<boolean>();
  @Output() goPage = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
  }

  onPrev(): void {
    this.goPrev.emit(true);
  }

  onNext(): void {
    this.goNext.emit(true);
  }

  onPage(n: number): void {
    this.goPage.emit(n);
  }

  totalPages(): number {
    return Math.ceil(this.count / this.perPage) || 0;
  }

  isLastPage(): boolean {
    return this.perPage * this.page >= this.count;
  }

  getMin(): number {
    return ((this.perPage * this.page) - this.perPage) + 1;
  }

  getMax(): number {
    let max = this.perPage * this.page;
    if (max > this.count) {
      max = this.count;
    }
    return max;
  }

  getPages(): number[] {
    const totalPages = Math.ceil(this.count / this.perPage);
    const thisPage = this.page || 1;
    const pagesToShow = this.pagesToShow || 9;
    const pages: number[] = [];
    pages.push(thisPage);

    console.log('Empezaando for con:', totalPages, 'Pagina:', thisPage, 'A mostrar:', pagesToShow );
    for (let i = 0; i < pagesToShow - 1; i++) {
      console.log('pages[]:', pages);
      if (pages.length < pagesToShow) {
        if (Math.min.apply(null, pages) > 1) {
          pages.push(Math.min.apply(null, pages) - 1);
          console.log('pushing', Math.min.apply(null, pages) - 1, 'al array');
        }
      }

      if (pages.length < pagesToShow) {
        if (Math.max.apply(null, pages) < totalPages) {
          pages.push(Math.max.apply(null, pages) + 1);
          console.log('pushing', Math.max.apply(null, pages) + 1, 'al array');
        }
      }
    }
    pages.sort((a, b) => a - b);
    return pages;
  }
}


/*


import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-paginacion',
  templateUrl: './paginacion.component.html',
  styleUrls: ['./paginacion.component.css']
})
export class PaginacionComponent implements OnInit {

  @Input() page: number;
  @Input() count: number;
  @Input() perPage: number;
  @Input() pagesToShow: number;
  @Input() loading: boolean;

  @Output() goPrev = new EventEmitter<boolean>();
  @Output() goNext = new EventEmitter<boolean>();
  @Output() goPage = new EventEmitter<number>(); //($event)

  
  constructor() { }

  ngOnInit() {
  }

  onPrev(): void{
    this.goPrev.emit(true);
  }

  onNext(): void{
    this.goNext.emit(true);
  }

  onPage(n: number): void{
    this.goPage.emit(n);
  }

 
  totalPages(): number{
    return Math.ceil(this.count / this.perPage) || 0;
  }

  //ultima pagina? (disable next?)
  isLastPage(): boolean{
    return this.perPage * this.page >= this.count;
  }

  getPages(): number[]{
    const totalPages = Math.ceil(this.count / this.perPage);
    const thisPage = this.page || 1 ;
    const pagesToShow = this.pagesToShow || 9;
    const pages: number[] = [];

    pages.push(this.page);

    for(let i = 0; i < pagesToShow - 1; i++){
      if(pages.length < pagesToShow){
        //el mínimo del array
        if(Math.min.apply(null, pages) > 1){
          pages.push(Math.min.apply(null, pages) - 1);
        }
      }
      if(pages.length < pagesToShow){
        if(Math.min.apply(null, pages) < totalPages){
          pages.push(Math.max.apply(null, pages) + 1);
        }
      }
      //min a max
      pages.sort((a,b) => a - b);
      return pages;
    }
  }

  getMin() : number{
    return ((this.perPage * this.page) - this.perPage + 1);
  }

  getMax() : number{
    var max = this.perPage * this.page;
    if(max > this.count){
      max = this.count;
    }
    return max;
  }


}
*/