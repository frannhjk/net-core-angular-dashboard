import { Component, OnInit } from '@angular/core';
import { Order } from './order'; 
//import { SalesDataService } from '../../services/sales-data.service';


// var getActualDate = function(){
//    var utc = new Date().toJSON().slice(0,10).replace(/-/g,'/');
//    return new Date(utc);
// }

@Component({
  selector: 'app-section-order',
  templateUrl: './section-order.component.html',
  styleUrls: ['./section-order.component.css']
})

export class SectionOrderComponent implements OnInit {
/*
  constructor(private _salesData: SalesDataService) { }

  orders: Order[];
  total = 0;
  page = 1;
  limit = 1;
  loading = false;

  ngOnInit() {
    this.getOrders();
  }
  
  getOrders(): void {
    this._salesData.getOrders(this.page, this.limit)
      .subscribe(res => {
        console.log("resultado getOrders: ", res);
        this.orders = res['page']['data'];
        this.total = res['page'].total;
        this.loading = false;
        //this.orders = res;
      });
  }

  //Anterior página
  goToPrevious(): void{
    this.page--;
    this.getOrders();
  }

  //Siguiente página
  goToNext(): void{
    this.page++;
    this.getOrders();
  }

  //Yendo a una página específica
  goToPage(n: number): void{
    this.page = n;
    this.getOrders();
  }



    /* ESTATICO - EJEMPLO */
    // orders: Order[] = [
    //   {id: 1, customer: {id: 1, name: 'Fabrica Saint Miguel', email: 'ventas@saintmiguel.com', city: 'El talar'}, total: 10500, placed: new Date('2018-09-05'), fulfilled: getActualDate()},
    //   {id: 2, customer: {id: 2, name: 'Todo Consumo', email: 'todoconsumo@gmail.com', city: 'Cardales'}, total: 25300, placed: getActualDate(), fulfilled: getActualDate()},
    //   {id: 3, customer: {id: 3, name: 'Coto', email: 'ventasinterior@coto.com', city: 'Capital Federal'}, total: 57000, placed: getActualDate(), fulfilled: getActualDate()},
    //   {id: 4, customer: {id: 4, name: 'Megaconsumo', email: 'contacto.megaconsumo@gmail.com', city: 'Arrecifes'}, total: 35000, placed: getActualDate(), fulfilled: getActualDate()},
    //   {id: 5, customer: {id: 5, name: 'San Cayetano', email: 'sancayetanomarket@hotmail.com', city: 'Capitán Sarmiento'}, total: 105000, placed: getActualDate( ), fulfilled: getActualDate()}, 
    //   {id: 6, customer: {id: 4, name: 'Dia %', email: 'ventas.dia@gmail.com', city: 'Pergamino'}, total: 44000, placed: getActualDate(), fulfilled: getActualDate()},
    //   {id: 7, customer: {id: 4, name: 'Wallmart', email: 'contacto.wallmart@gmail.com', city: 'Capital Federal'}, total: 120000, placed: getActualDate(), fulfilled: getActualDate()},
    // ];*/
    constructor(){}

    ngOnInit() {
    }
}
