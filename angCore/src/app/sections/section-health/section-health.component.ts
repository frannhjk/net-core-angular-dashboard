import { Component, OnInit } from '@angular/core';
import { Server } from '../../shared/server';

const SAMPLE_SERVERS = [
  {id: 1,   name: 'S - Cap Sto',    isOnline: true},    //sv 1
  {id: 1,   name: 'S - Pergamino',  isOnline: false},   //sv 2
  {id: 1,   name: 'S - Cap Fed',    isOnline: true},    //sv 3
  {id: 1,   name: 'S - Pilar',      isOnline: true},    //sv 4
];

@Component({
  selector: 'app-section-health',
  templateUrl: './section-health.component.html',
  styleUrls: ['./section-health.component.css']
})
export class SectionHealthComponent implements OnInit {

  constructor() { }

  servers: Server[] = SAMPLE_SERVERS;

  ngOnInit() {
  }

}
