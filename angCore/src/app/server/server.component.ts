import { Component, OnInit, Input } from '@angular/core';
import { Server } from '../shared/server';

@Component({
  selector: 'app-server',
  templateUrl: './server.component.html',
  styleUrls: ['./server.component.css']
})
export class ServerComponent implements OnInit {

  constructor() { }

  color: string;
  btnText: string;

  //pasarle la data a server
  @Input() serverInput: Server;

  ngOnInit() {
    this.setServerAction(this.serverInput.isOnline);
  }

  setServerAction(isOnline: boolean){
    if(isOnline){
      this.serverInput.isOnline = true;
      this.color = '#66BB6A';
      this.btnText = 'Apagar';
    }
    else{
      this.serverInput.isOnline = false;
      this.color = '#FF6B6B';
      this.btnText = 'Encender';
    }
  }

  cambiarEstado(onlineStatus: boolean){
    console.log(this.serverInput.name,':',onlineStatus);
    //this.serverInput.isOnline = !this.serverInput.isOnline;
    this.setServerAction(!onlineStatus); //change color
  }


 
}
